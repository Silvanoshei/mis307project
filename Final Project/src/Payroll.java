import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;


public class Payroll {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	throws IOException, SQLException, ClassNotFoundException
	{
		ArrayList<Employees> list = new ArrayList<Employees>();
		
		Scanner in = new Scanner(System.in);
		System.out.print("Welcome to the Payroll System!" + "\n" +"\n");
		System.out.print("MAIN MENU:" + "\n");
		System.out.print("1.  Upload Existing Data. \n");
		System.out.print("2.  Create New Database. \n \n");
		System.out.print("Please type in 1 or 2: ");
		
		// Fixed the bug with entering a letter.
		while(!in.hasNextInt()) {
			System.out.print("Whoops, please enter a number 1 or 2: ");		
			in.next();
		}
		
		int mainMenu = in.nextInt();
		
		// Checking to see if they entered the right number.
		while (mainMenu < 1 || mainMenu > 2) {
			System.out.print("Please enter 1 or 2 only: ");					
			mainMenu = in.nextInt();
		}
		
		// Construct an old database from a text file.
		if (mainMenu == 1) {
			System.out.print("Please enter your filename: ");
			String oldData = in.nextLine();
			
			File textUpload = new File(oldData);
			
			// Need to fix bug with wrong text file.
			Scanner textScan = new Scanner(textUpload);
			
			while (textScan.hasNextLine()) {
				
				String worker = textScan.next();
				
				// Create new HourlyWorker.
				if (worker.contains("HourlyWorker")) {
					
					// Grab rest of nextlines info.
					String firstName = textScan.next();
					String lastName = textScan.next();
					// Build full Name.
					String fullName = firstName + " " + lastName;
					int ssn = textScan.nextInt();
					int addressNum = textScan.nextInt();
					String addressName = textScan.next();
					// Build full Address.
					String fullAddress = addressNum + " " + addressName;
					String married = textScan.next();
					
					HourlyWorker newHW = new HourlyWorker(worker, fullName, ssn, fullAddress, married);
					list.add(newHW);
					
				}
				
				// Create new CommisionedWorker.
				if (worker.contains("ComissionedWorker")) {
					
					// Grab rest of nextlines info.
					String firstName = textScan.next();
					String lastName = textScan.next();
					// Build full Name.
					String fullName = firstName + " " + lastName;
					int ssn = textScan.nextInt();
					int addressNum = textScan.nextInt();
					String addressName = textScan.next();
					// Build full Address.
					String fullAddress = addressNum + " " + addressName;
					String married = textScan.next();
					
					CommisionedWorker newCW = new CommisionedWorker(worker, fullName, ssn, fullAddress, married);
					list.add(newCW);
					
				}
				
				// Create new SalaryWorker.
				if (worker.contains("SalaryWorker")) {
					
					// Grab rest of nextlines info.
					String firstName = textScan.next();
					String lastName = textScan.next();
					// Build full Name.
					String fullName = firstName + " " + lastName;
					int ssn = textScan.nextInt();
					int addressNum = textScan.nextInt();
					String addressName = textScan.next();
					// Build full Address.
					String fullAddress = addressNum + " " + addressName;
					String married = textScan.next();
					
					SalaryWorker newSW = new SalaryWorker(worker, fullName, ssn, fullAddress, married);
					list.add(newSW);
					
				}
				
				textScan.nextLine();
				
			}
		}
		
		if (mainMenu == 2) {
			System.out.print("TEST OK");
		}
		
		

	}

}
