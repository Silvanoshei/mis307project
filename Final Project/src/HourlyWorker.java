
public class HourlyWorker {
	private String worker;
	private String fullName;
	private int ssn;
	private String fullAddress;
	private String married;
	private double hourlyRate;
	private double hours;
	public HourlyWorker(String worker,String fullName, int ssn, String fullAddress, String married, double hourlyRate, double hours) {
		this.worker = worker;
		this.fullName = fullName;
		this.ssn = ssn;
		this.fullAddress = fullAddress;
		this.married = married;
		this.hourlyRate = hourlyRate;
		this.hours = hours;
	}
	
	public double hourlyWorkerGrossPay() {
		return hours * hourlyRate;
	}
	
	public double hourlyWorkerNetPay() {
		
		return 0;
	}
	
	public double getHours() {
		return hours;
	}
	
	public double federalIncomeTax() {
		double fedTaxRate = 0.10;
		double totalFedTax = (hours * hourlyRate) * fedTaxRate;
		return totalFedTax;
	}
	
	public double socialSecurityTax() {
		double socialSecurityRate = 0.062;
		double totalSecTax = (hours * hourlyRate) * socialSecurityRate;
		return totalSecTax;
	}
	
	public double medicareTax() {
		double medicareTaxRate = 0.0145;
		double totalMedTax = (hours * hourlyRate) * medicareTaxRate;
		return totalMedTax;
	}
	
	public double stateTax() {
		double iowaTaxRate = 0.0898;
		double stateTax = (hours * hourlyRate) * iowaTaxRate;
		return stateTax;
	}
	
	
}
